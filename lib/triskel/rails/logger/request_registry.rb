# frozen_string_literal: true

require 'rails/version'

module Triskel
  module Rails
    module Logger
      class RequestRegistry
        if ::Rails::VERSION::MAJOR == 4
          extend ActiveSupport::PerThreadRegistry
          attr_accessor :correlation_id
        else
          require 'active_support/core_ext/module/attribute_accessors_per_thread'
          thread_mattr_accessor :correlation_id, instance_accessor: false
        end
      end
    end
  end
end
