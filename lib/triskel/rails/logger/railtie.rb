# frozen_string_literal: true

require 'rails/railtie'

module Triskel
  module Rails
    module Logger
      class Railtie < ::Rails::Railtie
        Triskel::Rails::Logger.add_tagged_logging_formater_config

        initializer 'triskel.rails.logger.init', after: :load_config_initializers do |app|
          Triskel::Rails::Logger.setup(app)
        end
      end
    end
  end
end
