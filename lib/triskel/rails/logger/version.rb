# frozen_string_literal: true

# :nocov:
module Triskel
  module Rails
    module Logger
      VERSION = '1.4.0'
    end
  end
end
# :nocov:
