# frozen_string_literal: true

require 'ougai'
require 'mono_logger'

module Triskel
  module Rails
    module Logger
      class Logger < ::Ougai::Logger
        if defined?(ActiveSupport::LoggerThreadSafeLevel)
          # Needs to be included if exists in current rails version
          include ActiveSupport::LoggerThreadSafeLevel
        end
        include LoggerSilence

        def initialize(logdev, *args)
          options = args.extract_options!
          @use_readable_formatter = options[:use_readable_formatter] || false
          super

          after_initialize if respond_to? :after_initialize

          @logdev = MonoLogger::LocklessLogDevice.new(logdev) if options[:no_locks]

          @before_log = lambda do |data|
            transform_msg_in_message(data)
            include_request_correlation_id(data)
          end
        end

        def create_formatter
          formatter = if @use_readable_formatter
                        ::Ougai::Formatters::Readable.new(nil, nil, excluded_fields: [:level_name])
                      else
                        ::Ougai::Formatters::Bunyan.new
                      end
          formatter.datetime_format = '%FT%T%:z'
          formatter
        end

        private

        def transform_msg_in_message(data)
          return unless data.key?(:msg)

          data[:message] = data[:msg]
          data.delete(:msg)
        end

        def include_request_correlation_id(data)
          correlation_id = Triskel::Rails::Logger::RequestRegistry.correlation_id
          data[:correlation_id] = correlation_id if correlation_id.present?
        end

        def write(severity, args, fields, hooks)
          # ideally we should do this in the @before_log lambda, but we haven't there the severity field
          fields = include_level_name_if_not_present(severity, fields)
          super(severity, args, fields, hooks)
        rescue StandardError
          msg, ex, data = args
          super(severity, [clean_uploaded_files_params(as_hash(msg)), ex, data], fields, hooks)
        end

        def include_level_name_if_not_present(severity, fields)
          return fields if fields[:level_name].present?

          weak_merge!({ level_name: format_severity(severity) }, fields)
        end

        def clean_uploaded_files_params(data)
          data.transform_values do |v|
            if value_with_tempfile_attribute?(v)
              v.to_hash.exclude(:tempfile)
            elsif v.is_a?(Hash) || v.respond_to?(:to_hash)
              clean_uploaded_files_params(v)
            else
              v
            end
          end
        end

        def value_with_tempfile_attribute?(value)
          value.respond_to?(:tempfile) && value.respond_to?(:to_hash)
        end
      end
    end
  end
end
