# frozen_string_literal: true

module Triskel
  module Rails
    module Logger
      module CoreExt
        module ControllerAdditions
          def append_info_to_payload(payload)
            super

            payload[:ip] = request.remote_ip
            payload[:host] = request.host
            payload[:params] = request.filtered_parameters
            payload[:user_id] = current_user.id if defined?(current_user) && current_user.present?
          end

          def save_correlation_id
            Triskel::Rails::Logger::RequestRegistry.correlation_id = request.headers['X-Correlation-ID']
          end

          def self.prepended(base)
            base.before_action :save_correlation_id
          end
        end
      end
    end
  end
end
