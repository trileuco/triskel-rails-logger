# frozen_string_literal: true

require 'action_controller/metal/exceptions'

module Triskel
  module Rails
    module Logger
      module CoreExt
        module LogNotFoundAsInfo
          def log_error(env, wrapper)
            request = env.is_a?(Hash) ? ActionDispatch::Request.new(env) : env
            exception = wrapper.exception
            if exception.is_a? ActionController::RoutingError
              level = :info
              data = {
                time: Time.now.to_datetime,
                method: request.method,
                path: request.path,
                level_name: level.to_s.upcase,
                http_code: wrapper.status_code,
                msg: "#{exception.class.name}: #{exception.message}",
                log_type: 'request',
                ip: request.remote_ip,
                host: request.host
              }

              correlation_id = request.headers['X-Correlation-ID']
              Triskel::Rails::Logger::RequestRegistry.correlation_id = correlation_id
              data[:correlation_id] = correlation_id if correlation_id.present?

              logger(env).send(level, data)
            else
              super(env, wrapper)
            end
          end
        end
      end
    end
  end
end
