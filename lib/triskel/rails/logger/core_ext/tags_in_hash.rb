# frozen_string_literal: true

module Triskel
  module Rails
    module Logger
      module CoreExt
        module TagsInHash
          def call(severity, time, progname, data)
            if is_a?(::Ougai::Formatters::Base) && respond_to?(:current_tags)
              data = { msg: data.to_s } unless data.is_a?(Hash)
              tags = current_tags
              data[:tags] = tags if tags.present?
              _call(severity, time, progname, data)
            else
              super(severity, time, progname, data)
            end
          end
        end
      end
    end
  end
end
