# frozen_string_literal: true

require 'action_dispatch'
require 'active_support'
require 'rails/version'
require 'lograge'
require 'lograge/sql'

require 'triskel/rails/logger/version'
require 'triskel/rails/logger/logger'
require 'triskel/rails/logger/request_registry'
require 'triskel/rails/logger/core_ext/controller_additions'
require 'triskel/rails/logger/core_ext/log_not_found_as_info'
require 'triskel/rails/logger/core_ext/tags_in_hash'

module Triskel
  module Rails
    module Logger
      class << self
        mattr_accessor :config
        self.config = ActiveSupport::OrderedOptions.new

        def add_tagged_logging_formater_config
          ActiveSupport::TaggedLogging::Formatter.prepend Triskel::Rails::Logger::CoreExt::TagsInHash
        end

        def configure(&block)
          load_default_config
          block.call config if block_given?
        end

        def setup(app)
          return unless config.enabled

          app.config.colorize_logging = false

          satinize_config
          add_controller_additions
          add_lograge_config(app)
          add_lograge_sql_config(app) if config.log_sql_queries
        end

        private

        def load_default_config
          config.enabled = false
          config.base_controller_class = nil
          config.ignore_actions = nil
          config.ignore_custom = nil
          config.keys_for_request_msg = %w[method path format controller action status params]
          config.excluded_params_for_request_msg = %w[controller action format base64_image attachment]
          config.log_sql_queries = false
        end

        def satinize_config
          config.keys_for_request_msg = Array(config.keys_for_request_msg || []).map(&:to_sym)
          config.excluded_params_for_request_msg = Array(config.excluded_params_for_request_msg || []).map(&:to_s)
        end

        def add_controller_additions
          base_classes = Array(config.base_controller_class)
          base_classes.map! { |klass| klass.try(:constantize) }

          base_classes.each do |base_class|
            extend_base_class(base_class)
          end
        end

        def extend_base_class(base_class)
          return unless defined? base_class

          base_class.class_eval do
            prepend Triskel::Rails::Logger::CoreExt::ControllerAdditions
          end
        end

        def add_lograge_config(app)
          app.config.lograge.enabled = true
          app.config.lograge.formatter = Lograge::Formatters::Raw.new
          app.config.lograge.base_controller_class = config.base_controller_class
          app.config.lograge.ignore_actions = config.ignore_actions
          app.config.lograge.ignore_custom = config.ignore_custom
          app.config.lograge.custom_options = lambda do |event|
            http_code = event.payload[:status].to_i
            {
              time: Time.now.to_datetime,
              level_name: lograge_level_name(http_code),
              http_code: http_code,
              msg: lograge_msg(event),
              ip: event.payload[:ip],
              host: event.payload[:host],
              params: event.payload[:params],
              user_id: event.payload[:user_id],
              data: {
                exception: event.payload[:exception],
                exception_object: event.payload[:exception_object]
              },
              log_type: 'request'
            }
          end
          add_lograge_not_found_errors_patch
        end

        def lograge_level_name(http_code)
          http_code >= 500 ? 'ERROR' : 'INFO'
        end

        def lograge_msg(event)
          payload_for_msg = event.payload.slice(*config.keys_for_request_msg)
          payload_for_msg[:params].except!(*config.excluded_params_for_request_msg) if payload_for_msg[:params].present?

          exception = event.payload[:exception]
          exception.present? ? exception[1] : Lograge::Formatters::KeyValue.new.call(payload_for_msg)
        end

        def add_lograge_not_found_errors_patch
          ActionDispatch::DebugExceptions.prepend Triskel::Rails::Logger::CoreExt::LogNotFoundAsInfo
        end

        def add_lograge_sql_config(app)
          require_lograge_sql_extension

          app.config.lograge_sql.extract_event = proc { |event|
            { name: event.payload[:name], duration: event.duration.to_f.round(2), sql: event.payload[:sql] }
          }

          app.config.lograge_sql.formatter = proc { |sql_queries|
            sql_queries
          }
        end

        def require_lograge_sql_extension
          require 'active_record/log_subscriber'
          require 'lograge/sql/extension'
        end
      end
    end
  end
end

require 'triskel/rails/logger/railtie' if defined?(Rails)
