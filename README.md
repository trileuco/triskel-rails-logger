# Triskel::Rails::Logger

This gem includes some common classes and utilities to format the log of the Rails applications with the expected format of the Triskel logging architecture.

## Installation

Add this line to your application's Gemfile (adapting the tag to the one you wish to use):

```ruby
gem 'triskel-rails-logger', git: 'git@bitbucket.org:trileuco/triskel-rails-logger.git', tag: 'v1.0.0'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install triskel-rails-logger

## Usage

To configure the gem in your rails application you must do two actions:

  1. Create an initializer, for example `config/initializers/triskel_logger.rb` and add to it a content similar to the following one, adapting each configuration parameter to your needs:

      ```ruby
      Triskel::Rails::Logger.configure do |config|
        config.enabled = Rails.env.production? || Settings.logger.try(:use_triskel_logger)
        config.log_sql_queries = ENV['LOG_SQL_QUERIES'] || Settings.logger.try(:use_triskel_logger_sql_queries)
        config.base_controller_class = ['ActionController::Base', 'ActionController::API']
        config.ignore_actions = %w[OkComputer::OkComputerController#index OkComputer::OkComputerController#show]
        config.ignore_custom = lambda do |event|
          # return true here if you want to ignore based on the ActiveSupport::Notifications::Event
        end
        config.keys_for_request_msg = %w[method path format controller action status params]
        config.excluded_params_for_request_msg = %w[controller action format base64_image attachment]
      end
      ```

      * The `enabled` field is used to enabled all the triskel-rails-logger configuration.

      * The `log_sql_queries` field is used to configure or not the lograge sql extension, that includes all the queries executed in each request in the message log resuming the request.

      * The `base_controller_class` field can be set with a simple class name or with an array of class names, that will be patched by lograge to log all its request and for this gem to configure before filters to save the `X-Correlation-ID` header value (to log in all the log lines of the request) and for add to the request payload son fields like the ip, host, filtered_params, user_id, ...

      * The `keys_for_request_msg` field specifies the list of keys from the lograge data Hash, that has to be in the json of the request log.

      * The `excluded_params_for_request_msg` field specifies the list of params of the request that are excluded from the params key of the lograge data Hash. It only applies when the params key is included on the `keys_for_request_msg` config field.

  2. Create the logger instance and set it as the rails logger. You can do this in the `config/application.rb` or in the environment file `config/environment/production.rb`, `config/environment/development.rb`, etc

      ```ruby
      ...
      logger = Triskel::Rails::Logger::Logger.new(STDOUT)
      config.logger = ActiveSupport::TaggedLogging.new(logger)
      ...
      ```

      In development environments you may want to use a configuration like this:

      ```ruby
      if Settings.logger.try(:use_triskel_logger)
        output_file = Rails.root.join("log/#{Rails.env}.log")
        file_logger = Triskel::Rails::Logger::Logger.new(output_file, use_readable_formatter: true)
        config.logger = ActiveSupport::TaggedLogging.new(logger)
      end
      ```

      where the use_readable_formatter allows to use a formatter that outputs the log in a more readable way.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To test the code with different combinations of gem versions (in this case just with Rails 4 and with Rails 5) we use the gemika gem. To configure it you can use the following command, this will install the gems needed for each Gemfile of the project based on the .travis.yml file:

```bash
bundle exec rake matrix:install
```

Finally we can execute the tests with each combination of gems using the following command (that will also will be the default executing just `rake`):

```bash
bundle exec rake matrix:spec
```

## Code syntax and conventions checks

We use overcommit for automated git hooks that validate code syntax and conventions. To configure overcommit go to the project root and execute:

```bash
bundle install --gemfile=.overcommit_gems.rb
overcommit --install
```

Check that everything is working correctly with:

```bash
overcommit -r
```

Now before each commit the validations will be executed and if something isn't correct the commit won't be done.

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/trileuco/triskel-rails-logger. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Triskel::Rails::Logger project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/trileuco/triskel-rails-logger/src/master/CODE_OF_CONDUCT.md).
