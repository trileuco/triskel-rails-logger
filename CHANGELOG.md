# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.4.0] - 2021-11-11

### Added

- [TTR-1340](https://redmine.trileuco.com/issues/TTR-1340)
  - Add new no_lock option to protect against trapped contexts

## [v1.3.0] - 2021-01-27

### Changed

- [TSSERVICES-242](https://redmine.trileuco.com/issues/TSSERVICES-242)
  - Use message field instead of msg for log messages

## [v1.2.0] - 2020-12-30

### Added

- [TSSERVICES-231](https://redmine.trileuco.com/issues/TSSERVICES-231)
  - Add support for lograge ignore_custom config option on Triskel Rails Logger

## [v1.1.0] - 2020-09-10

### Added

- [TSSERVICES-187](https://redmine.trileuco.com/issues/TSSERVICES-187)
  - Add support on Triskel Rails Logger for Rails 6

## [v1.0.3] - 2020-04-25
### Fixed
- Avoid error when log requests with binary files being uploaded in its params

## [v1.0.2] - 2020-04-16
### Changed
- Readable formatter uses now gem amazing_print instead of awesome_print

### Removed
- Remove support of Ruby 2.3

## [v1.0.1] - 2020-02-06
### Fixed
- Fix bug when current_user method is defined but returns nil

## [v1.0.0] - 2020-02-06
### Added
- Initial version of Triskel Rails Logger gem
- Configuration of overcommit and rubocop gems to check code syntax and conventions
- Configure rspec, simplecov, guard and gemika gems to do the testing of the project
- This gem automatically adds the gems (lograge and ougai) configuration for that ones that use to gets the logging format required by the Triskel logging architecture
- Log the 404 error requests as INFO level to avoid that it generate unuseful alerts in the monitoring
systems.
- Add the logic to get the value of the 'X-Correlation-ID' header to be included in all the log lines of
a request if the header is present.
