# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
RSpec.shared_context 'controller_notification_events' do
  let(:event_params) { ActiveSupport::HashWithIndifferentAccess.new(foo: 'bar', fuu: 'bur') }
  let(:ok_event) do
    ActiveSupport::Notifications::Event.new(
      'process_action.action_controller',
      Time.now,
      Time.now,
      1,
      status: 200,
      controller: 'HomeController',
      action: 'index',
      format: 'application/json',
      method: 'GET',
      path: '/home?foo=bar&fuu=bur',
      params: event_params,
      db_runtime: 0.02,
      view_runtime: 0.01,
      ip: '192.168.0.10',
      host: 'my.app.com',
      user_id: '1234'
    )
  end
  let(:ko_event) do
    ActiveSupport::Notifications::Event.new(
      'process_action.action_controller',
      Time.now,
      Time.now,
      2,
      status: '500',
      exception: [StandardError, 'an error'],
      exception_object: StandardError.new
    )
  end
  let(:not_found_event) do
    ActiveSupport::Notifications::Event.new(
      'process_action.action_controller',
      Time.now,
      Time.now,
      3,
      status: 404
    )
  end
end

RSpec.shared_context 'sql_notification_events' do
  let(:sql_query_event) do
    start = Time.now
    ActiveSupport::Notifications::Event.new(
      'sql.active_record',
      start,
      start + 0.023234,
      4,
      name: 'A name for the query',
      sql: 'select 1'
    )
  end
end
# rubocop:enable Metrics/BlockLength
