# frozen_string_literal: true

RSpec.shared_context 'custom_options common behavior' do
  specify 'the response is a hash' do
    expect(custom_option_response).to be_a Hash
  end

  specify 'the response has a time' do
    expect(custom_option_response[:time]).not_to be_nil
  end

  specify 'the response has a log_type' do
    expect(custom_option_response[:log_type]).to eq 'request'
  end
end
