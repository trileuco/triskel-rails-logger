# frozen_string_literal: true

RSpec.shared_context 'a controller with the controller additions included' do
  specify 'the append_info_to_payload method mantains the fields declared on the payload' do
    expect(subject[:timestamp]).to eq(Date.parse('5-11-1955'))
  end

  specify 'the append_info_to_payload method adds the fields declared on the base controller' do
    expect(subject[:base_appended]).to be true
  end

  specify 'the append_info_to_payload method adds the ip field declared on the request' do
    expect(subject[:ip]).to eq('192.168.10.10')
  end

  specify 'the append_info_to_payload method adds the host field declared on the request' do
    expect(subject[:host]).to eq('host.name')
  end

  specify 'the append_info_to_payload method adds the filtered_params field declared on the request' do
    expect(subject[:params]).to eq(password: '[FILTERED]', login: 'username')
  end
end

RSpec.shared_context 'a controller prepended with ControllerAdditions module' do
  let(:controller) { nil }

  it 'is prepended with the ControllerAdditions module methods' do
    base_class_index = controller.ancestors.index(controller)
    additions_class_index = controller.ancestors.index(Triskel::Rails::Logger::CoreExt::ControllerAdditions)
    expect(base_class_index).not_to be_nil
    expect(additions_class_index).not_to be_nil
    expect(additions_class_index).to be < base_class_index
  end
end
