# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
RSpec.describe Triskel::Rails::Logger::CoreExt::TagsInHash do
  let(:ougai_formatter_class) do
    Class.new(::Ougai::Formatters::Bunyan) do
      prepend Triskel::Rails::Logger::CoreExt::TagsInHash
    end
  end
  let(:simple_formatter_class) do
    Class.new(::ActiveSupport::Logger::SimpleFormatter) do
      prepend Triskel::Rails::Logger::CoreExt::TagsInHash
    end
  end

  describe '.call' do
    let(:time) { DateTime.now }

    context 'when the formatter is an subclass of Ougai::Formatters::Base' do
      context 'when it includes the ActiveSupport::TaggedLogging::Formatter' do
        before { ougai_formatter_class.include ActiveSupport::TaggedLogging::Formatter }
        let(:formatter) { ougai_formatter_class.new }

        context 'when data param is not a Hash' do
          it 'outputs a JSON with the fields: name, time, msg and tags (with the tags passed to tagged method' do
            formatter.tagged(%w[TAG1 TAG2]) do
              json_output = JSON.parse(formatter.call(:info, time, 'c', 'd'))
              expect(json_output).to include('time', 'name' => 'c', 'msg' => 'd', 'tags' => %w[TAG1 TAG2])
            end
          end
        end

        context 'when data param is a Hash' do
          it 'outputs a JSON with the fields: name, time, msg and tags (with the tags passed to tagged method' do
            formatter.tagged(%w[TAG1 TAG2]) do
              json_output = JSON.parse(formatter.call(:info, time, 'c', msg: 'd'))
              expect(json_output).to include('time', 'name' => 'c', 'msg' => 'd', 'tags' => %w[TAG1 TAG2])
            end
          end
        end
      end

      context 'when it does not includes the ActiveSupport::TaggedLogging::Formatter' do
        let(:formatter) { ougai_formatter_class.new }

        context 'when data param is not a Hash' do
          it 'outputs a JSON with the fields: name, time, msg but no tags' do
            json_output = JSON.parse(formatter.call(:info, time, 'c', 'd'))
            expect(json_output).to include('time', 'name' => 'c', 'msg' => 'd')
            expect(json_output).not_to include('tags')
          end
        end

        context 'when data param is a Hash' do
          it 'outputs a JSON with the fields: name, time, msg but no tags' do
            json_output = JSON.parse(formatter.call(:info, time, 'c', msg: 'd'))
            expect(json_output).to include('time', 'name' => 'c', 'msg' => 'd')
            expect(json_output).not_to include('tags')
          end
        end
      end
    end

    context 'when the formatter is not an subclass of Ougai::Formatters::Base' do
      context 'when it includes the ActiveSupport::TaggedLogging::Formatter' do
        before { simple_formatter_class.include ActiveSupport::TaggedLogging::Formatter }

        let(:formatter) { simple_formatter_class.new }

        context 'when data param is not a Hash' do
          it 'outputs the string prefixed with the tags' do
            formatter.tagged(%w[TAG1 TAG2]) do
              expect(formatter.call(:info, time, 'c', 'd')).to start_with '[TAG1] [TAG2] d'
            end
          end
        end

        context 'when data param is a Hash' do
          it 'outputs the Hash transformed in string prefixed with the tags' do
            formatter.tagged(%w[TAG1 TAG2]) do
              expect(formatter.call(:info, time, 'c', msg: 'd')).to start_with '[TAG1] [TAG2] {:msg=>"d"}'
            end
          end
        end
      end

      context 'when it does not includes the ActiveSupport::TaggedLogging::Formatter' do
        let(:formatter) { simple_formatter_class.new }

        context 'when data param is not a Hash' do
          it 'outputs the string prefixed without the tags' do
            expect(formatter.call(:info, time, 'c', 'd')).to start_with 'd'
          end
        end

        context 'when data param is a Hash' do
          it 'outputs the Hash transformed in string prefixed with the tags' do
            expect(formatter.call(:info, time, 'c', msg: 'd')).to start_with '{:msg=>"d"}'
          end
        end
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
