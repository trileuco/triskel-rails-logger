# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
RSpec.describe Triskel::Rails::Logger::CoreExt::LogNotFoundAsInfo do
  let(:logger_double) do
    double('logger').tap do |logger|
      allow(logger).to receive(:info).with(anything)
    end
  end
  let(:debug_exceptions_class) do
    Class.new do
      def initialize(logger)
        @logger = logger
      end

      def log_error(_env, _wrapper)
        delegating_method
      end

      def delegating_method; end

      def logger(_env)
        @logger
      end
    end
  end

  before do
    debug_exceptions_class.class_eval do
      prepend Triskel::Rails::Logger::CoreExt::LogNotFoundAsInfo
    end
  end

  after { Triskel::Rails::Logger::RequestRegistry.correlation_id = nil }

  describe '.log_error' do
    let(:debug_exception) { debug_exceptions_class.new(logger_double) }

    context 'when param wrapper has not a exception field' do
      let(:request) { instance_double(ActionDispatch::Request) }
      let(:wrapper_object) { double('wrapper', exception: nil) }

      it 'delegates in the super log_error method' do
        expect(debug_exception).to receive(:delegating_method)
        debug_exception.log_error(request, wrapper_object)
      end
    end

    context 'when param wrapper has a exception field of type different to ActionController::RountingError' do
      let(:request) { instance_double(ActionDispatch::Request) }
      let(:wrapper_object) { double('wrapper', exception: ActionController::RenderError.new('render error')) }

      it 'delegates in the super log_error method' do
        expect(debug_exception).to receive(:delegating_method)
        debug_exception.log_error(request, wrapper_object)
      end
    end

    context 'when param wrapper has a exception field of type ActionController::RountingError' do
      let(:request) do
        instance_double('ActionDispatch::Request', method: 'GET', path: '/path', remote_ip: '192.168.10.10',
                                                   host: 'my.app.com', headers: {})
      end

      let(:wrapper_object) do
        double('wrapper', exception: ActionController::RoutingError.new('not found'), status_code: 404)
      end

      it 'does not delegate in the super log_error method' do
        expect(debug_exception).not_to receive(:delegating_method)
        debug_exception.log_error(request, wrapper_object)
      end

      it 'calls to the logger info method' do
        expect(logger_double).to receive(:info).with(
          hash_including(:time, method: 'GET', path: '/path', level_name: 'INFO', http_code: 404,
                                message: 'ActionController::RoutingError: not found', log_type: 'request',
                                ip: '192.168.10.10', host: 'my.app.com')
        ).with(hash_excluding(:correlation_id))
        debug_exception.log_error(request, wrapper_object)
      end

      it 'saves in the RequestRegistry.correlation_id the value of the request header "X-Correlation-ID"' do
        expect(Triskel::Rails::Logger::RequestRegistry.correlation_id).to be_nil
      end

      context 'when the request includes a header with the correlation id' do
        let(:request) do
          instance_double('ActionDispatch::Request', method: 'GET', path: '/path', remote_ip: '192.168.10.10',
                                                     host: 'my.app.com').tap do |r|
            allow(r).to receive(:headers).and_return('X-Correlation-ID' => 'asdf-ghjk-poiu-ytre')
          end
        end

        it 'calls to the logger info method with correlation_id field' do
          expect(logger_double).to receive(:info).with(hash_including(correlation_id: 'asdf-ghjk-poiu-ytre'))
          debug_exception.log_error(request, wrapper_object)
        end

        it 'saves in the RequestRegistry.correlation_id the value of the request header "X-Correlation-ID"' do
          debug_exception.log_error(request, wrapper_object)
          expect(Triskel::Rails::Logger::RequestRegistry.correlation_id).to eq 'asdf-ghjk-poiu-ytre'
        end
      end
    end

    context 'when param env is not a request but a env hash' do
      let(:env) do
        { 'REQUEST_METHOD' => 'GET', 'PATH_INFO' => '/path', 'REMOTE_ADDR' => '192.168.10.10',
          'HTTP_HOST' => 'my.app.com' }
      end

      let(:wrapper_object) do
        double('wrapper', exception: ActionController::RoutingError.new('not found'), status_code: 404)
      end

      it 'calls to the logger info method' do
        expect(logger_double).to receive(:info).with(
          hash_including(:time, method: 'GET', path: '/path', level_name: 'INFO', http_code: 404,
                                message: 'ActionController::RoutingError: not found', log_type: 'request',
                                ip: '192.168.10.10', host: 'my.app.com')
        ).with(hash_excluding(:correlation_id))
        debug_exception.log_error(env, wrapper_object)
      end

      it 'saves in the RequestRegistry.correlation_id the value of the request header "X-Correlation-ID"' do
        expect(Triskel::Rails::Logger::RequestRegistry.correlation_id).to be_nil
      end

      context 'when the env includes a header with the correlation id' do
        let(:env_with_correlation_id) do
          env.merge!('HTTP_X_CORRELATION_ID' => 'poiu-ytre-asdf-ghjk')
        end

        it 'calls to the logger info method with correlation_id field' do
          expect(logger_double).to receive(:info).with(hash_including(correlation_id: 'poiu-ytre-asdf-ghjk'))
          debug_exception.log_error(env_with_correlation_id, wrapper_object)
        end

        it 'saves in the RequestRegistry.correlation_id the value of the request header "X-Correlation-ID"' do
          debug_exception.log_error(env_with_correlation_id, wrapper_object)
          expect(Triskel::Rails::Logger::RequestRegistry.correlation_id).to eq 'poiu-ytre-asdf-ghjk'
        end
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
