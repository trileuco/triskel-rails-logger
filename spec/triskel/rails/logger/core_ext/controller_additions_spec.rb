# frozen_string_literal: true

require 'support/controller_additions_examples'
require 'support/matchers/have_filters'

# rubocop:disable Metrics/BlockLength
RSpec.describe Triskel::Rails::Logger::CoreExt::ControllerAdditions do
  let(:base_controller_class) { 'ActionController::Base' }
  let(:base_controller) do
    Class.new do
      def self.before_action(*args); end

      def append_info_to_payload(payload)
        payload.merge!(base_appended: true)
      end

      def request
        ActiveSupport::OrderedOptions.new.tap do |request|
          request.remote_ip = '192.168.10.10'
          request.host = 'host.name'
          request.filtered_parameters = { password: '[FILTERED]', login: 'username' }
        end
      end
    end
  end
  let(:payload) { { timestamp: Date.parse('5-11-1955') } }

  subject { payload.dup }

  before do
    base_controller.class_eval do
      prepend Triskel::Rails::Logger::CoreExt::ControllerAdditions
    end
  end

  context 'with a BaseController with the module prepended' do
    before { base_controller.new.append_info_to_payload(subject) }

    it_behaves_like 'a controller with the controller additions included'
  end

  context 'with an ApplicationController extending the BaseController with the module prepended' do
    let(:application_controller) do
      Class.new(base_controller) do
        def append_info_to_payload(payload)
          super
          payload.merge!(app_appended: true)
        end
      end
    end

    context 'when the application controller has not a current_user method defined' do
      before { application_controller.new.append_info_to_payload(subject) }

      it_behaves_like 'a controller with the controller additions included'

      specify 'the append_info_to_payload method adds the fields declared on the application controller' do
        expect(subject[:app_appended]).to be true
      end
    end

    context 'when the application controller has a current_user method defined but returns nil' do
      before do
        application_controller.class_eval do
          def current_user
            nil
          end
        end
        application_controller.new.append_info_to_payload(subject)
      end

      it_behaves_like 'a controller with the controller additions included'

      specify 'the append_info_to_payload method adds the fields declared on the application controller' do
        expect(subject[:app_appended]).to be true
      end
    end

    context 'when the application controller has a current_user method defined' do
      before do
        application_controller.class_eval do
          def current_user
            ActiveSupport::OrderedOptions.new.tap do |user|
              user.id = '24601'
            end
          end
        end
        application_controller.new.append_info_to_payload(subject)
      end

      it_behaves_like 'a controller with the controller additions included'

      specify 'the append_info_to_payload method adds the current_user.id field declared on the request' do
        expect(subject[:user_id]).to eq('24601')
      end
    end
  end

  context 'when a controller is prepended with the ControllerAdditions' do
    let(:controller_class) do
      Class.new(ActionController::Base) do
        prepend Triskel::Rails::Logger::CoreExt::ControllerAdditions

        def request
          ActiveSupport::OrderedOptions.new.tap do |request|
            request.headers = { 'X-Correlation-ID' => 'asdfghjkl' }
          end
        end
      end
    end

    it 'adds a before_filter save_correlation_id to the extended controllers' do
      expect(controller_class).to have_filters(:before, :save_correlation_id)
    end

    describe '#save_correlation_id' do
      after { Triskel::Rails::Logger::RequestRegistry.correlation_id = nil }

      it 'saves in the RequestRegistry.correlation_id the value of the request header "X-Correlation-ID"' do
        controller_class.new.save_correlation_id
        expect(Triskel::Rails::Logger::RequestRegistry.correlation_id).to eq 'asdfghjkl'
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
