# frozen_string_literal: true

RSpec.describe Triskel::Rails::Logger::Railtie do
  subject { Triskel::Rails::Logger::Railtie }

  it { is_expected.to be < Rails::Railtie }

  it 'has one initializer to be executed after load_config_initializers' do
    expect(Triskel::Rails::Logger::Railtie.initializers.size).to eq(1)
    expect(Triskel::Rails::Logger::Railtie.initializers[0].after).to be(:load_config_initializers)
  end

  it 'the initializer calls to the Triskel::Rails::Logger setup method' do
    expect(Triskel::Rails::Logger).to receive(:setup)
    Triskel::Rails::Logger::Railtie.initializers.each(&:run)
  end
end
