# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
RSpec.describe Triskel::Rails::Logger::Logger do
  let(:io) { StringIO.new }
  subject { described_class.new(io) }

  let(:item) do
    log_str = io.string
    begin
      JSON.parse(log_str, symbolize_names: true)
    rescue StandardError
      nil
    end
  end

  it 'includes the LoggerSilence module' do
    expect(described_class.ancestors).to include(LoggerSilence)
  end

  it 'includes the ActiveSupport::LoggerThreadSafeLevel module' do
    expect(described_class.ancestors).to include(ActiveSupport::LoggerThreadSafeLevel)
  end

  describe '.new' do
    context 'when no specify initializer options' do
      it 'sets Bunyan to formatter attribute' do
        expect(subject.formatter).to be_an(Ougai::Formatters::Bunyan)
      end

      it 'sets log device to standard' do
        expect(subject.instance_variable_get('@logdev')).to be_a(Logger::LogDevice)
      end
    end

    context 'when the use_readable_formatter options is set to true in initializer options' do
      subject { described_class.new(io, use_readable_formatter: true) }

      it 'sets Readable to formatter attribute' do
        expect(subject.formatter).to be_an(Ougai::Formatters::Readable)
      end
    end

    context 'when no_locks is set to true' do
      subject { described_class.new(io, no_locks: true) }

      it 'sets log device to a Monologger::LocklessDevice instance' do
        expect(subject.instance_variable_get('@logdev')).to be_a(MonoLogger::LocklessLogDevice)
      end
    end
  end

  shared_examples 'log' do
    context 'with message' do
      it 'returns a json with the message' do
        subject.send(method, log_msg)
        expect(item).to include(message: log_msg)
      end

      it 'adds the level_name field to the result json' do
        subject.send(method, log_msg)
        expect(item).to include(level_name: method.to_s.upcase)
      end

      context 'when the correlation_id has been set on the RequestRegistry' do
        before { Triskel::Rails::Logger::RequestRegistry.correlation_id = 'asdf' }
        after { Triskel::Rails::Logger::RequestRegistry.correlation_id = nil }

        it 'adds the correlation_id field to the result json' do
          subject.send(method, log_msg)
          expect(item).to include(correlation_id: 'asdf')
        end
      end

      context 'when the correlation_id has not been set on the RequestRegistry' do
        it 'does not add the correlation_id field to the result json' do
          subject.send(method, log_msg)
          expect(item).not_to include(:correlation_id)
        end
      end

      context 'with readable formatter' do
        subject! { described_class.new(io, use_readable_formatter: true) }

        it 'does not add the level_name field to the result json' do
          subject.send(method, log_msg)
          expect(io.string).not_to include('level_name')
        end
      end
    end

    context 'with data' do
      it 'returns a json with the message and the other data' do
        subject.send(method, msg: log_msg, data_id: 108, action: 'dump', level_name: 'DEBUG')
        expect(item).to include(message: log_msg, data_id: 108, action: 'dump', level_name: 'DEBUG')
      end

      it 'does not fails if a UploadedFile is in the params field of data' do
        uploaded_file = Rack::Test::UploadedFile.new('spec/fixtures/random_bin')
        subject.send(method, msg: log_msg, data_id: 108, action: 'dump', params: { file: uploaded_file })
        expect(item).to include(message: log_msg, data_id: 108, action: 'dump', params: hash_including(:file))
      end

      it 'adds the level_name field to the result json' do
        subject.send(method, msg: log_msg, data_id: 108, action: 'dump')
        expect(item).to include(level_name: method.to_s.upcase)
      end

      it 'formats the time field with the format "%FT%T%:z"' do
        time = DateTime.now
        subject.send(method, msg: log_msg, time: time)
        expect(item).to include(time: time.strftime('%FT%T%:z'))
      end

      context 'when the correlation_id has been set on the RequestRegistry' do
        before { Triskel::Rails::Logger::RequestRegistry.correlation_id = 'asdf' }
        after { Triskel::Rails::Logger::RequestRegistry.correlation_id = nil }

        it 'adds the correlation_id field to the result json' do
          subject.send(method, log_msg)
          expect(item).to include(correlation_id: 'asdf')
        end
      end

      context 'when the correlation_id has not been set on the RequestRegistry' do
        it 'does not add the correlation_id field to the result json' do
          subject.send(method, log_msg)
          expect(item).not_to include(:correlation_id)
        end
      end

      context 'with readable formatter' do
        subject! { described_class.new(io, use_readable_formatter: true) }

        it 'does not add the level_name field to the result json' do
          subject.send(method, msg: log_msg, data_id: 108, action: 'dump')
          expect(io.string).not_to include('level_name')
        end
      end
    end
  end

  describe '#trace' do
    before { subject.level = :trace }

    let(:log_msg) { 'trace message' }
    let(:method) { 'trace' }

    it_behaves_like 'log'
  end

  describe '#debug' do
    let(:log_msg) { 'debug message' }
    let(:method) { 'debug' }

    it_behaves_like 'log'
  end

  describe '#info' do
    let(:log_msg) { 'info message' }
    let(:method) { 'info' }

    it_behaves_like 'log'
  end

  describe '#warn' do
    let(:log_msg) { 'warn message' }
    let(:method) { 'warn' }

    it_behaves_like 'log'
  end

  describe '#error' do
    let(:log_msg) { 'error message' }
    let(:method) { 'error' }

    it_behaves_like 'log'
  end

  describe '#fatal' do
    let(:log_msg) { 'fatal message' }
    let(:method) { 'fatal' }

    it_behaves_like 'log'
  end
end
# rubocop:enable Metrics/BlockLength
