# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
RSpec.describe Triskel::Rails::Logger::RequestRegistry do
  subject { Triskel::Rails::Logger::RequestRegistry }

  it 'has a correlation_id attribute initialized to nil' do
    Thread.new do
      expect(subject.correlation_id).to be_nil
    end.join
  end

  context 'when a value is set for the correlation_id' do
    specify 'the correlation_id value can be read' do
      Thread.new do
        subject.correlation_id = 'asdf'
        expect(subject.correlation_id).to eq 'asdf'
      end.join
    end

    specify 'the correlation_id value cannot be read in other threads' do
      threads = []
      threads << Thread.new do
        subject.correlation_id = 'things'
        Thread.pass
        expect(subject.correlation_id).to eq 'things'
      end

      threads << Thread.new do
        subject.correlation_id = 'other things'
        Thread.pass
        expect(subject.correlation_id).to eq 'other things'
      end

      threads << Thread.new do
        subject.correlation_id = 'really other things'
        Thread.pass
        expect(subject.correlation_id).to eq 'really other things'
      end

      threads.each(&:join)
    end
  end
end
# rubocop:enable Metrics/BlockLength
