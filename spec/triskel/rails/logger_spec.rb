# frozen_string_literal: true

require 'action_controller'
require 'support/controller_additions_examples'
require 'support/custom_options_examples'
require 'support/notification_events_examples'

# rubocop:disable Metrics/BlockLength
RSpec.describe Triskel::Rails::Logger do
  subject { Triskel::Rails::Logger }

  let(:app) do
    double(config: ActiveSupport::OrderedOptions.new.tap do |config|
                     config.lograge = ActiveSupport::OrderedOptions.new
                     config.lograge.enabled = false
                     config.lograge_sql = ActiveSupport::OrderedOptions.new
                   end)
  end

  it 'has a version number' do
    expect(subject::VERSION).not_to be nil
  end

  context 'the ActiveSupport::TaggedLogging::Formatter module' do
    it 'is prepended with the TagsInHash module' do
      ancestors = ActiveSupport::TaggedLogging::Formatter.ancestors
      base_class_index = ancestors.index(ActiveSupport::TaggedLogging::Formatter)
      additions_class_index = ancestors.index(Triskel::Rails::Logger::CoreExt::TagsInHash)
      expect(base_class_index).not_to be_nil
      expect(additions_class_index).not_to be_nil
      expect(additions_class_index).to be < base_class_index
    end
  end

  describe '.configure' do
    it { expect { |b| subject.configure(&b) }.to yield_with_args(ActiveSupport::OrderedOptions) }

    context 'when no block is defined' do
      before { subject.configure }

      it { is_expected.to have_attributes(config: have_attributes(enabled: false)) }
      it { is_expected.to have_attributes(config: have_attributes(base_controller_class: nil)) }
      it { is_expected.to have_attributes(config: have_attributes(ignore_actions: nil)) }
      it { is_expected.to have_attributes(config: have_attributes(ignore_custom: nil)) }
      it do
        is_expected.to(have_attributes(config: have_attributes(
          keys_for_request_msg: %w[method path format controller action status params]
        )))
      end
      it do
        is_expected.to(have_attributes(config: have_attributes(
          excluded_params_for_request_msg: %w[controller action format base64_image attachment]
        )))
      end
      it { is_expected.to have_attributes(config: have_attributes(log_sql_queries: false)) }
    end

    context 'when a block is defined to change the configuration' do
      before do
        subject.configure do |config|
          config.enabled = true
          config.base_controller_class = 'ActionController::Base'
          config.ignore_actions = %w[OkComputer::OkComputerController#index OkComputer::OkComputerController#show]
          config.ignore_custom = ->(event) { event.payload[:status].to_i == 501 }
          config.keys_for_request_msg = %w[method path status]
          config.excluded_params_for_request_msg = %w[controller action]
          config.log_sql_queries = true
        end
      end

      it { is_expected.to have_attributes(config: have_attributes(enabled: true)) }
      it { is_expected.to have_attributes(config: have_attributes(base_controller_class: 'ActionController::Base')) }
      it do
        is_expected.to(have_attributes(config: have_attributes(
          ignore_actions: %w[OkComputer::OkComputerController#index OkComputer::OkComputerController#show]
        )))
      end
      it { is_expected.to(have_attributes(config: have_attributes(ignore_custom: a_truthy_value))) } # a not nil value
      it { is_expected.to(have_attributes(config: have_attributes(keys_for_request_msg: %w[method path status]))) }
      it do
        is_expected.to(have_attributes(config: have_attributes(excluded_params_for_request_msg: %w[controller action])))
      end
      it { is_expected.to have_attributes(config: have_attributes(log_sql_queries: true)) }
    end
  end

  describe '.setup' do
    context 'with the default config' do
      before do
        subject.configure
        subject.setup(app)
      end

      specify 'the colorize_logging app config is nil' do
        expect(app.config.colorize_logging).to be_nil
      end

      specify 'the lograge config is not activate' do
        expect(app.config.lograge.enabled).to be false
      end
    end

    context 'when the default config has been changed' do
      context 'when the config.enable field has been set to true' do
        include_context 'controller_notification_events'

        before do
          subject.configure do |config|
            config.enabled = true
            config.base_controller_class = 'ActionController::Base'
            config.ignore_actions = %w[OkComputer::OkComputerController#index OkComputer::OkComputerController#show]
            config.ignore_custom = ->(event) { event.payload[:status].to_i == 501 }
            config.keys_for_request_msg = %w[method path status]
            config.excluded_params_for_request_msg = %w[controller action]
            config.log_sql_queries = true
          end
          subject.setup(app)
        end

        specify 'the app colorize_logging config is disabled' do
          expect(app.config.colorize_logging).to be false
        end

        specify 'the lograge config is enabled' do
          expect(app.config.lograge.enabled).to be true
        end

        specify 'the lograge formatter is the Lograge Raw' do
          expect(app.config.lograge.formatter).to be_a(Lograge::Formatters::Raw)
        end

        specify 'the lograge base_controller_class is the configured for the Triskel Logger config' do
          expect(app.config.lograge.base_controller_class).to eq(subject.config.base_controller_class)
        end

        specify 'the lograge ignore_actions is the configured for Triskel Logger config' do
          expect(app.config.lograge.ignore_actions).to eq(subject.config.ignore_actions)
        end

        specify 'the lograge ignore_custom is the configured for Triskel Logger config' do
          expect(app.config.lograge.ignore_custom).to eq(subject.config.ignore_custom)
        end

        specify 'the lograge custom_options is defined and accepts a notification event' do
          expect(app.config.lograge.custom_options(ok_event)).not_to be_nil
        end

        context 'the ActionDispatch::DebugExceptions module' do
          it 'is prepended with the LogNotFoundAsInfo module' do
            ancestors = ActionDispatch::DebugExceptions.ancestors
            base_class_index = ancestors.index(ActionDispatch::DebugExceptions)
            additions_class_index = ancestors.index(Triskel::Rails::Logger::CoreExt::LogNotFoundAsInfo)
            expect(base_class_index).not_to be_nil
            expect(additions_class_index).not_to be_nil
            expect(additions_class_index).to be < base_class_index
          end
        end
      end
    end

    describe 'the lograge custom_options' do
      include_context 'controller_notification_events'

      context 'when is called for a ok event' do
        before do
          subject.configure do |config|
            config.enabled = true
            config.keys_for_request_msg = %w[method path status params]
            config.excluded_params_for_request_msg = %w[controller action]
          end
          subject.setup(app)
        end

        let(:custom_option_response) { app.config.lograge.custom_options.call(ok_event) }

        include_examples 'custom_options common behavior'

        specify 'the response has a level_name equal to INFO' do
          expect(custom_option_response[:level_name]).to eq('INFO')
        end

        specify 'the response has a http_code equal to 200' do
          expect(custom_option_response[:http_code]).to eq(200)
        end

        specify 'the response has a ip field' do
          expect(custom_option_response[:ip]).not_to be_blank
        end

        specify 'the response has a host field' do
          expect(custom_option_response[:host]).not_to be_blank
        end

        specify 'the response has a host field' do
          expect(custom_option_response[:params]).not_to be_blank
        end

        specify 'the response has a user_id field' do
          expect(custom_option_response[:user_id]).not_to be_blank
        end

        specify 'the response has not a data[:exception]' do
          expect(custom_option_response[:data][:exception]).to be_nil
          expect(custom_option_response[:data][:exception_object]).to be_nil
        end

        describe 'the response msg field' do
          it 'is a message describing the request' do
            expect(custom_option_response[:msg]).to eq(
              'method=GET path=/home?foo=bar&fuu=bur status=200 params={"foo"=>"bar", "fuu"=>"bur"}'
            )
          end

          context 'when a field is remove from the keys_for_request_msg config' do
            before do
              subject.configure do |config|
                config.enabled = true
                config.keys_for_request_msg = %w[method path params]
                config.excluded_params_for_request_msg = %w[controller action fuu]
              end
              subject.setup(app)
            end

            it 'is not showed in the msg' do
              expect(custom_option_response[:msg]).not_to match(/status=/)
            end
          end

          context 'when a params is included in the excluded_params_for_request_msg config' do
            before do
              subject.configure do |config|
                config.enabled = true
                config.keys_for_request_msg = %w[method params]
                config.excluded_params_for_request_msg = %w[controller action fuu]
              end
              subject.setup(app)
            end

            it 'is not showed in the msg' do
              expect(custom_option_response[:msg]).to match(/params=\{/)
              expect(custom_option_response[:msg]).not_to match(/"fuu"=>/)
            end
          end
        end
      end

      context 'when is called for a ko event' do
        before do
          subject.configure do |config|
            config.enabled = true
            config.keys_for_request_msg = %w[method path status params]
            config.excluded_params_for_request_msg = %w[controller action fuu]
          end
          subject.setup(app)
        end

        let(:custom_option_response) { app.config.lograge.custom_options.call(ko_event) }

        include_examples 'custom_options common behavior'

        specify 'the response has a level_name equal to ERROR' do
          expect(custom_option_response[:level_name]).to eq('ERROR')
        end

        specify 'the response has a http_code equal to 500' do
          expect(custom_option_response[:http_code]).to eq(500)
        end

        specify 'the response has a msg not blank' do
          expect(custom_option_response[:msg]).to eq('an error')
        end

        specify 'the response has a data[:exception]' do
          expect(custom_option_response[:data][:exception]).not_to be_nil
          expect(custom_option_response[:data][:exception_object]).not_to be_nil
        end
      end

      context 'when is called for a not_found event' do
        before do
          subject.configure do |config|
            config.enabled = true
            config.keys_for_request_msg = %w[method path status]
            config.excluded_params_for_request_msg = %w[controller action]
          end
          subject.setup(app)
        end

        let(:custom_option_response) { app.config.lograge.custom_options.call(not_found_event) }

        include_examples 'custom_options common behavior'

        specify 'the response has a level_name equal to INFO' do
          expect(custom_option_response[:level_name]).to eq('INFO')
        end

        specify 'the response has a http_code equal to 404' do
          expect(custom_option_response[:http_code]).to eq(404)
        end
      end
    end

    describe 'the lograge_sql configuration' do
      context 'with the default config' do
        before { subject.configure }

        specify 'the Lograge::Sql::Extension is not defined' do
          expect(subject).not_to receive(:require_lograge_sql_extension)
          subject.setup(app)
        end

        specify 'the lograge_sql configuration parameters are not set' do
          subject.setup(app)
          expect(app.config.lograge_sql.extract_event).to be_nil
          expect(app.config.lograge_sql.formatter).to be_nil
        end
      end

      context 'when the configure method activate the enabled and log_sql_queries parameters' do
        before do
          subject.configure do |config|
            config.enabled = true
            config.log_sql_queries = true
          end
        end

        specify 'the Lograge::Sql::Extension is defined' do
          expect(subject).to receive(:add_lograge_sql_config).and_call_original
          subject.setup(app)
        end

        it 'adds one Lograge::ActiveRecordLogSubscriber subscriber for sql.active_record' do
          subject.setup(app)
          subscribers = ActiveSupport::LogSubscriber.log_subscribers
          lograge_subscribers = subscribers.select { |ls| ls.is_a?(Lograge::ActiveRecordLogSubscriber) }
          expect(lograge_subscribers.size).to eq(1)
          expect(lograge_subscribers[0].patterns).to include('sql.active_record')
        end

        context 'when the setup method is called' do
          before { subject.setup(app) }

          include_context 'sql_notification_events'

          specify 'lograge_sql is configured' do
            expect(app.config.lograge_sql.extract_event).not_to be_nil
            expect(app.config.lograge_sql.formatter).not_to be_nil
          end

          specify 'lograge_sql extract_event is a lambda returning a hash with the name, sql and duration of the sql' do
            extract_event_result = app.config.lograge_sql.extract_event.call(sql_query_event)
            expect(extract_event_result).to be_a(Hash)
            expect(extract_event_result[:name]).to eq(sql_query_event.payload[:name])
            expect(extract_event_result[:sql]).to eq(sql_query_event.payload[:sql])
            expect(extract_event_result[:duration]).to eq(23.23)
          end

          it 'lograge_sql formatter is a lambda returning exactly the recieved param' do
            sql_queries = [{ name: 'sql1', sql: 'select 1', duration: 10 },
                           { name: 'sql2', sql: 'select 2', duracion: 30 }]
            formatter_result = app.config.lograge_sql.formatter.call(sql_queries)
            expect(formatter_result).to be(sql_queries)
          end
        end
      end
    end

    describe 'the base_controller_class config parameter' do
      let(:a_base_controller_class) { 'ActionController::Base' }
      let(:a_base_controller) { Class.new(ActionController::Base) }

      before { stub_const(a_base_controller_class, a_base_controller) }

      context 'when is defined with an class name' do
        before do
          subject.configure do |config|
            config.enabled = true
            config.base_controller_class = a_base_controller_class
          end
          subject.setup(app)
        end

        context 'the base_controller' do
          it_should_behave_like 'a controller prepended with ControllerAdditions module' do
            let(:controller) { a_base_controller }
          end
        end
      end

      context 'when is defined with an array of class names' do
        let(:other_base_controller_class) { 'ActionController::Api' }
        let(:other_base_controller) { Class.new(ActionController::Base) }

        before do
          stub_const(other_base_controller_class, other_base_controller)
          subject.configure do |config|
            config.enabled = true
            config.base_controller_class = [a_base_controller_class, other_base_controller_class]
          end
          subject.setup(app)
        end

        context 'the base_controller' do
          it_should_behave_like 'a controller prepended with ControllerAdditions module' do
            let(:controller) { a_base_controller }
          end
        end
        context 'the other_base_controller' do
          it_should_behave_like 'a controller prepended with ControllerAdditions module' do
            let(:controller) { other_base_controller }
          end
        end
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
